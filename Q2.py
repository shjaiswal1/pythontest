def combinations(com):
    if com == "":
        return []
    D = {
        "1": "abc",
        "2": "def",
        "3": "ghi",
        "4": "jkl",
        "5": "mno",
        "6": "pqrs",
        "7": "tuv",
        "8": "wxy",
        "9": "z"
        }
    result = [""]
    for i in com:
        temp = []
        for j in result:
            for char in D[i]:
                temp.append(j+ char)
        result = temp
    return result

print(combinations("33"))