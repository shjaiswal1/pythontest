def bitwise(x, y):
    a = (x & y) << 1;
    res = x^y;
    if (a== 0):
        return res;
    return bitwise(a, res);


print(bitwise(2, 4));
